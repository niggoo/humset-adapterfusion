from common_visualization import *
import re

def get_combined_attention_visualization(layer_scores_per_xlm_roberta: dict, layer_scores_per_xtreme_distil: dict):
    fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(12, 5))

    layer_scores_per_dicts = [layer_scores_per_xlm_roberta, layer_scores_per_xtreme_distil]
    titles = ["$XLM-R_{Base}$", "$XtremeDistil_{l6-h256}$"]

    for i, ax in enumerate(axs):
        layer_scores_per = layer_scores_per_dicts[i]
        scores_per = list(map(lambda x: x.mean(axis=(1)), layer_scores_per.values()))
        scores = np.vstack(scores_per).transpose()

        rounded_scores = np.around(scores, 2)
        sns.heatmap(scores, annot=rounded_scores, ax=ax)

        labels = list(layer_scores_per.keys())
        ax.set_yticks(np.arange(0.5, len(labels)+0.5))
        ax.set_xticks(np.arange(0.5, len(labels)+0.5))
        ax.set_yticklabels(labels, rotation=45)
        ax.set_xticklabels(labels, rotation=45)
        if i == 0:
            ax.set_ylabel('Adapters')
        ax.set_xlabel('Tasks')

        ax.set_title(titles[i])

    plt.tight_layout()
    plt.savefig("combined_attention.jpg", dpi=300)



fusions_to_load_xtreme_distil = {
    "sectors": "adapterfusion_microsoft-xtremedistil-l6-h256-uncased_sectors_run_1682149823",
    "pillars_1d": "adapterfusion_microsoft-xtremedistil-l6-h256-uncased_pillars_1d_run_1682264332",
    "pillars_2d": "adapterfusion_microsoft-xtremedistil-l6-h256-uncased_pillars_2d_run_1682270835",
    "subpillars_1d": "adapterfusion_microsoft-xtremedistil-l6-h256-uncased_subpillars_1d_run_1682300735",
    "subpillars_2d": "adapterfusion_microsoft-xtremedistil-l6-h256-uncased_subpillars_2d_run_1682284587"
}

fusions_to_load_xlm_roberta = {
    "sectors": "adapterfusion_xlm-roberta-base_sectors_run_1683442435",
    "pillars_1d": "adapterfusion_xlm-roberta-base_pillars_1d_run_1683650423",
    "pillars_2d": "adapterfusion_xlm-roberta-base_pillars_2d_run_1683193731",
    "subpillars_1d": "adapterfusion_xlm-roberta-base_subpillars_1d_run_1683337347",
    "subpillars_2d": "adapterfusion_xlm-roberta-base_subpillars_2d_run_1683860757"
}

attention_scores_xlm_roberta = {}
attention_scores_xtreme_distil = {}

for fusion_key, fusion_value in fusions_to_load_xlm_roberta.items():
    attention_scores_xlm_roberta[fusion_key] = get_layered_attention_scores(load_attention_scores(fusion_value)["val"])

for fusion_key, fusion_value in fusions_to_load_xtreme_distil.items():
    attention_scores_xtreme_distil[fusion_key] = get_layered_attention_scores(load_attention_scores(fusion_value)["val"])

get_combined_attention_visualization(attention_scores_xlm_roberta, attention_scores_xtreme_distil)