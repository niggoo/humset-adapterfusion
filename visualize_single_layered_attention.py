import matplotlib.pyplot as plt

from common_visualization import *

def get_single_layered_attention_visualization(layer_scores, column, adapterfusion_name, model_name):
    width = layer_scores.shape[1]
    height = layer_scores.shape[0]

    plt.figure(figsize=(width, height))

    plt.title(model_name + "\n" + column)

    rounded_layer_scores = np.around(layer_scores, 2)

    ax = sns.heatmap(layer_scores, annot=rounded_layer_scores)

    xtick_labels = [f'Layer {i}' for i in range(layer_scores.shape[1])]
    ytick_labels = ['sectors', 'pillars_1d', 'pillars_2d', 'subpillars_1d', 'subpillars_2d']

    plt.yticks(np.arange(0.5, 5.5), ytick_labels)
    plt.xticks(np.arange(0.5, layer_scores.shape[1] + 0.5), xtick_labels)

    ax.set_yticklabels(ax.get_yticklabels(), rotation=45)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=45)

    plt.tight_layout()

    plt.savefig(f'./attention-scores/{adapterfusion_name}.png')
    plt.clf()


# fusions_to_load = {
#     "sectors": "adapterfusion_microsoft-xtremedistil-l6-h256-uncased_sectors_run_1682149823",
#     "pillars_1d": "adapterfusion_microsoft-xtremedistil-l6-h256-uncased_pillars_1d_run_1682264332",
#     "pillars_2d": "adapterfusion_microsoft-xtremedistil-l6-h256-uncased_pillars_2d_run_1682270835",
#     "subpillars_1d": "adapterfusion_microsoft-xtremedistil-l6-h256-uncased_subpillars_1d_run_1682300735",
#     "subpillars_2d": "adapterfusion_microsoft-xtremedistil-l6-h256-uncased_subpillars_2d_run_1682284587"
# }

fusions_to_load = {
    "sectors": "adapterfusion_xlm-roberta-base_sectors_run_1683442435",
    "pillars_1d": "adapterfusion_xlm-roberta-base_pillars_1d_run_1683650423",
    "pillars_2d": "adapterfusion_xlm-roberta-base_pillars_2d_run_1683193731",
    "subpillars_1d": "adapterfusion_xlm-roberta-base_subpillars_1d_run_1683337347",
    "subpillars_2d": "adapterfusion_xlm-roberta-base_subpillars_2d_run_1683860757"
}


for fusion_key, fusion_value in fusions_to_load.items():

    attention_scores = load_attention_scores(fusion_value)

    layer_scores = get_layered_attention_scores(attention_scores["test"])
    get_single_layered_attention_visualization(layer_scores, fusion_key, fusion_value, "$XLM-RoBERTa_{Base}$")



