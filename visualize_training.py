import re

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

files = [
    "xtreme-distil-sectors.csv",
    "xtreme-distil-pillars_1d.csv",
    "xtreme-distil-pillars_2d.csv",
    "xtreme-distil-subpillars_1d.csv",
    "xtreme-distil-subpillars_2d.csv"
]
model = "$XtremeDistil_{l6-h256}$"
ylim_values = [
    (0.15, 0.7),
    (0, 0.4),
    (0.2, 0.55),
    (0, 0.25),
    (0, 0.35)
]

files = [
    "xlm-roberta-sectors.csv",
    "xlm-roberta-pillars_1d.csv",
    "xlm-roberta-pillars_2d.csv",
    "xlm-roberta-subpillars_1d.csv",
    "xlm-roberta-subpillars_2d.csv"
]
model = "$XLM-R_{Base}$"
ylim_values = [
    (0.6, 0.75),
    (0.2, 0.55),
    (0.5, 0.65),
    (0.15, 0.5),
    (0.15, 0.5)
]


titles = ["Sectors", "Pillars 1D", "Pillars 2D", "Subpillars 1D", "Subpillars 2D"]

# Create subplots
fig, axes = plt.subplots(2, 3, figsize=(15, 7))

# Flatten the axes array for easier iteration
axes = axes.flatten()

# Remove the last unused subplot if there are only 5 plots
if len(files) < len(axes):
    fig.delaxes(axes[-1])  # remove the last axes

for i, ax in enumerate(axes[:len(files)]):
    # Load data from the CSV file
    df = pd.read_csv(files[i])
    df.columns = ['Step', 'AdapterFusion', 'AdapterFusion MIN', 'AdapterFusion MAX',
                  'Adapter', 'Adapter MIN', 'Adapter MAX']
    df = df[["AdapterFusion", "Adapter"]]

    # Plot the data
    sns.lineplot(data=df, ax=ax)

    ax.set_xlabel('Epochs')
    if i == 0 or i == 3:  # only set the ylabel for the first column
        ax.set_ylabel('Macro Average F1')
    ax.set_title(f"{model}\n{titles[i]}")
    ax.set_ylim(ylim_values[i])

plt.tight_layout()
plt.savefig("training_" + re.sub('[^0-9a-zA-Z]+', '', model) + ".jpg", dpi=300)