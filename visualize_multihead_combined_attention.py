from common_visualization import *
from matplotlib import colors

def get_combined_attention_visualization(attention_scores_xlm: np.array, attention_scores_xtreme: np.array):
    width = 5
    height = 5
    fig, axs = plt.subplots(1, 2, figsize=(width, height))

    cbar_ax = fig.add_axes([.91, .3, .03, .4])
    norm = colors.Normalize(vmin=0, vmax=0.5)

    for i, (attention_scores, title) in enumerate(zip([attention_scores_xlm, attention_scores_xtreme], ["$XLM-R_{Base}$", "$XtremeDistil_{l6-h256}$"])):
        scores = np.round(attention_scores.mean(axis=1).reshape((5,1)), 2)

        sns.heatmap(scores, annot=True, ax=axs[i], cbar=i == 0, cbar_ax=None if i else cbar_ax, norm=norm)
        axs[i].set_yticklabels(axs[i].get_yticklabels(), rotation=45)
        axs[i].set_xticklabels(axs[i].get_xticklabels())

        ytick_labels = ['sectors', 'pillars_1d', 'pillars_2d', 'subpillars_1d', 'subpillars_2d']
        axs[i].set_yticks(np.arange(0.5, 5.5))
        axs[i].set_yticklabels(ytick_labels)
        axs[i].set_xticks(np.arange(0.5, 1.5))
        axs[i].set_xticklabels(["Multihead"])

        axs[i].set_title(title)

    fig.tight_layout(rect=[0, 0, .9, 1])  # adjust the layout to leave space for the colorbar
    plt.savefig("multi_combined_attention.jpg", dpi=300)

fusion_name_xlm="adapterfusion_multi_head_xlm-roberta-base_run_1684064345"
fusion_name_xtreme="adapterfusion_multi_head_microsoft-xtremedistil-l6-h256-uncased_run_1682317682"

attention_scores_xlm = get_layered_attention_scores(load_attention_scores(fusion_name_xlm)["val"])
attention_scores_xtreme = get_layered_attention_scores(load_attention_scores(fusion_name_xtreme)["val"])

get_combined_attention_visualization(attention_scores_xlm, attention_scores_xtreme)
