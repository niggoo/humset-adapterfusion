import pickle
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import os

def load_attention_scores(adapterfusion_name):
    with open(f"./attention-scores/{adapterfusion_name}.bin", "rb") as h:
        return pickle.load(h)

def get_layered_attention_scores(scores):
    fusion_layer_name = "sectors,pillars_1d,pillars_2d,subpillars_1d,subpillars_2d"
    layer_results = []

    for layer in scores[0][fusion_layer_name].keys():
        result = np.vstack(list(map(lambda x: x[fusion_layer_name][layer]["output_adapter"].mean(axis=(0, 1)),
                           scores))).mean(axis=(0))
        layer_results.append(result)

    return np.vstack(layer_results).transpose()