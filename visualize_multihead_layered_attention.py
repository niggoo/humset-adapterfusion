import matplotlib.pyplot as plt

from common_visualization import *

def get_multihead_layered_attention_visualization(layer_scores, column, adapterfusion_name):
    width = layer_scores.shape[1]
    height = layer_scores.shape[0]
    plt.figure(figsize=(width, height))

    ax = sns.heatmap(layer_scores, annot=True)
    ax.set_yticklabels(ax.get_yticklabels(), rotation=45)
    plt.title(column)
    ytick_labels = ['sectors', 'pillars_1d', 'pillars_2d', 'subpillars_1d', 'subpillars_2d']
    plt.yticks(np.arange(0.5, 5.5), ytick_labels)

    xtick_labels = [f'Layer {i}' for i in range(layer_scores.shape[1])]
    plt.xticks(np.arange(0.5, layer_scores.shape[1] + 0.5), xtick_labels)
    plt.savefig(f'./attention-scores/{adapterfusion_name}.png')
    plt.clf()



fusion_name="adapterfusion_multi_head_xlm-roberta-base_run_1684064345"


attention_scores = load_attention_scores(fusion_name)

layer_scores = get_layered_attention_scores(attention_scores["test"])
get_multihead_layered_attention_visualization(layer_scores, "XLM Roberta Multihead", fusion_name)



