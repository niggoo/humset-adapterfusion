import sys

import pandas as pd
from torch import Tensor
import torch
from transformers.adapters import AutoAdapterModel
import wandb
import os
import torch
import numpy as np
import random
import ast
import pickle
import functools
from sklearn import preprocessing
from sklearn.dummy import DummyClassifier
from sklearn.metrics import classification_report
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler, Dataset
from torch.nn import BCEWithLogitsLoss, BCELoss
from tqdm import tqdm, trange
from sklearn.metrics import classification_report, confusion_matrix, multilabel_confusion_matrix, f1_score, accuracy_score
from transformers.adapters.composition import Fuse
import transformers
from datetime import datetime

wandb.login(key="")

def get_run_id():
    curr_dt = datetime.now()
    secs = int(round(curr_dt.timestamp()))

    return f"run_{secs}"

def seed_everything():
    np.random.seed(0)
    torch.manual_seed(0)
    random.seed(0)


df_train = pd.read_csv("humset/train.csv")
df_val = pd.read_csv("humset/val.csv")
df_test = pd.read_csv("humset/test.csv")

df = pd.read_csv("humset/humset_data.csv")


df_train = pd.merge(df_train, df[["entry_id", "pillars_1d", "pillars_2d"]], on='entry_id', how='left')
df_val = pd.merge(df_val, df[["entry_id", "pillars_1d", "pillars_2d"]], on='entry_id', how='left')
df_test = pd.merge(df_test, df[["entry_id", "pillars_1d", "pillars_2d"]], on='entry_id', how='left')

COLUMNS = [
    'sectors',
    'pillars_1d',
    'pillars_2d',
    'subpillars_2d',
    'subpillars_1d',
]

def pre_process(df):

    """
    python type list conversion and cleaning
    adding a clean_excerpt column with cleaned text on stopwords, nltk, and lemmatitazion
    using spacy pre-trained models

    """

    def clean_and_convert(col):
        if str(col)=="nan":
            col = "[]"
        if col[0]=="[" and col[-1]=="]":
            col = ast.literal_eval(col)
        else:
            col = [col]
        return [a for a in col if a not in ["None",
                                            "NOT_MAPPED",
                                            "UNKNOWN", None]]

    for c in COLUMNS:
        df[c] = df.apply(lambda x: clean_and_convert(x[c]), axis=1)

    return df

df_train = pre_process(df_train)
df_val = pre_process(df_val)
df_test = pre_process(df_test)

def combine_cols(df):
    return df["sectors"] + df["pillars_1d"] + df["pillars_2d"] + df["subpillars_1d"] + df["subpillars_2d"]

df_train['all'] = combine_cols(df_train)
df_val['all'] = combine_cols(df_val)
df_test['all'] = combine_cols(df_test)

if not os.path.exists("./metrics"):
    os.makedirs("./metrics")

if not os.path.exists("./adapters-output"):
    os.makedirs("./adapters-output")

if not os.path.exists("./adapter-fusions-output"):
    os.makedirs("./adapter-fusions-output")

if not os.path.exists("./attention-scores"):
    os.makedirs("./attention-scores")

if not os.path.exists("./predictions"):
    os.makedirs("./predictions")

STORAGE_PATH = "." # LOCAL



device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

if torch.cuda.is_available():
    print(torch.cuda.get_device_name(0))
else:
    print("Using cpu")

 # here selection which pre-trained language model to fine-tuned
XTREME_DISTL = "microsoft/xtremedistil-l6-h256-uncased"
XLM_ROBERTA = "xlm-roberta-base"
DISTIL_ROBERTA = "distilroberta-base"