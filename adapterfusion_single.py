from common import *
from common_adapterfusion import *


def train_adapterfusion(run_id: str, column: str, adapters: dict, backbone: str, learning_rate):
    seed_everything()
    print(f"Starting {run_id} adapterfusion training with <{backbone}> for <{column}>")

    # parameters
    epochs = 20
    max_stagnating_epochs = 5
    max_length = 100
    batch_size = 32
    adapterfusion_name = f"adapterfusion_{backbone}_{column}_{run_id}".replace("/", "-")

    threshold = 0.50

    gradient_accumulation_steps = 4

    wandb.init(
        project="humset-adapterfusion",
        name=adapterfusion_name,
        mode="online",
        config={
            "use_subset_only": False,
            "type": "adapterfusion",
            "backbone": backbone,
            "column": column,
            "run_id": run_id,
            "max_epochs": epochs,
            "max_stagnating_epochs": max_stagnating_epochs,
            "max_length": max_length,
            "batch_size": batch_size,
            "learning_rate": learning_rate,
            "threshold": threshold,
            "adapters": ', '.join(adapters.values()),
            "sequential": False,
            "gradient_accumulation_steps": gradient_accumulation_steps
        }
    )

    df_train_subset = df_train
    df_val_subset = df_val

    mlb = preprocessing.MultiLabelBinarizer()
    mlb.fit(df_train_subset[column])
    num_labels = len(list(mlb.classes_))

    Y_train = pd.DataFrame(mlb.transform(df_train_subset[column]), columns=list(mlb.classes_))
    Y_val = pd.DataFrame(mlb.transform(df_val_subset[column]), columns=list(mlb.classes_))

    tokenizer = transformers.AutoTokenizer.from_pretrained(
        backbone,
        do_lower_case=True,
        create_token_type_ids_from_sequences=True
    )

    # ADAPTERS BEGIN
    model = AutoAdapterModel.from_pretrained(
        backbone
    )
    model.add_classification_head(','.join(adapters.keys()), num_labels=num_labels, multilabel=True)

    for adapter_key, adapter_value in adapters.items():
        print(f"Loading adapter {adapter_value} for {adapter_key}")
        model.load_adapter(f'{STORAGE_PATH}/adapters-output/{adapter_value}/', load_as=f"{adapter_key}",
                           with_head=False)

    adapter_setup = Fuse(*list(adapters.keys()))

    print(f"Creating new adapterfusion-layer: {adapterfusion_name}")
    model.add_adapter_fusion(adapter_setup)

    model.set_active_adapters(adapter_setup)
    model.train_adapter_fusion(adapter_setup)

    print(model.adapter_summary())
    print(model.active_adapters)
    print(model.active_head)
    # ADAPTERS END

    model.to(device)

    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'gamma', 'beta']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)],
         'weight_decay_rate': 0.01},
        {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)],
         'weight_decay_rate': 0.0}
    ]

    optimizer = transformers.AdamW(optimizer_grouped_parameters, lr=learning_rate, correct_bias=True)

    encodings_train = tokenizer.batch_encode_plus(df_train_subset.excerpt.tolist(), max_length=max_length,
                                                  pad_to_max_length=True, truncation=True)
    encodings_val = tokenizer.batch_encode_plus(df_val_subset.excerpt.tolist(), max_length=max_length,
                                                pad_to_max_length=True, truncation=True)

    input_ids_train = encodings_train['input_ids']
    attention_masks_train = encodings_train['attention_mask']

    input_ids_val = encodings_val['input_ids']
    attention_masks_val = encodings_val['attention_mask']

    train_inputs = torch.tensor(input_ids_train)
    train_labels = torch.tensor(Y_train.to_numpy())
    train_masks = torch.tensor(attention_masks_train)

    validation_inputs = torch.tensor(input_ids_val)
    validation_labels = torch.tensor(Y_val.to_numpy())
    validation_masks = torch.tensor(attention_masks_val)

    train_data = TensorDataset(train_inputs, train_masks, train_labels)
    train_sampler = RandomSampler(train_data)
    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=batch_size)

    validation_data = TensorDataset(validation_inputs, validation_masks, validation_labels)
    validation_sampler = SequentialSampler(validation_data)
    validation_dataloader = DataLoader(validation_data, sampler=validation_sampler, batch_size=batch_size)

    best_val_f1 = 0
    stagnating_since = 0
    for epoch in trange(epochs, desc="Epoch"):
        # Training
        train_loss = 0
        true_labels, pred_labels = [], []

        model.train()
        print("Starting new Train iteration")
        for i, batch in enumerate(train_dataloader):
            i % 500 == 0 and print(f"Epoch {epoch}: TRAIN {i}/{len(train_dataloader)} ")

            batch = tuple(t.to(device) for t in batch)
            b_input_ids, b_input_mask, b_labels = batch

            outputs = model(b_input_ids, token_type_ids=None, attention_mask=b_input_mask)

            logits = outputs[0]

            loss = torch.nn.functional.binary_cross_entropy_with_logits(
                logits.view(-1, num_labels),
                b_labels.type_as(logits).view(-1, num_labels)
            )

            train_loss += loss.item()

            pred_label = torch.sigmoid(outputs[0]).to('cpu').detach().numpy()

            true_labels.append(b_labels.to('cpu').numpy())
            pred_labels.append(pred_label)

            loss.backward()

            if (i + 1) % gradient_accumulation_steps == 0:
                optimizer.step()
                optimizer.zero_grad()

        print("Finished training iteration, starting evaluation of train iteration")

        pred_labels = [item for sublist in pred_labels for item in sublist]
        true_labels = [item for sublist in true_labels for item in sublist]

        pred_bools = [pl > threshold for pl in pred_labels]
        true_bools = [tl == 1 for tl in true_labels]
        train_f1 = f1_score(true_bools, pred_bools, average='macro')

        # Validation
        val_loss = 0
        true_labels, pred_labels = [], []

        model.eval()
        print("Starting new Validation iteration")
        with torch.no_grad():
            for i, batch in enumerate(validation_dataloader):
                i % 100 == 0 and print(f"Epoch {epoch}: VAL {i}/{len(validation_dataloader)}")

                batch = tuple(t.to(device) for t in batch)
                b_input_ids, b_input_mask, b_labels = batch

                outputs = model(b_input_ids, token_type_ids=None, attention_mask=b_input_mask)

                logits = outputs[0]

                loss = torch.nn.functional.binary_cross_entropy_with_logits(
                    logits.view(-1, num_labels),
                    b_labels.type_as(logits).view(-1, num_labels)
                )

                val_loss += loss.item()

                pred_label = torch.sigmoid(outputs[0]).to('cpu').numpy()

                true_labels.append(b_labels.to('cpu').numpy())
                pred_labels.append(pred_label)
            print("Finished validation iteration, starting evaluation of validation iteration")

        pred_labels = [item for sublist in pred_labels for item in sublist]
        true_labels = [item for sublist in true_labels for item in sublist]

        pred_bools = [pl > threshold for pl in pred_labels]
        true_bools = [tl == 1 for tl in true_labels]
        val_f1 = f1_score(true_bools, pred_bools, average='macro')
        print("Logging to wandb")
        wandb.log({
            'train/loss': train_loss,
            'train/macro_average_f1': train_f1,
            'val/loss': val_loss,
            'val/macro_average_f1': val_f1,
        })

        # Early Stopping
        if val_f1 > best_val_f1:
            best_val_f1 = val_f1
            stagnating_since = 0
            # ADAPTERS BEGIN
            model.save_adapter_fusion(f"{STORAGE_PATH}/adapter-fusions-output/{adapterfusion_name}",
                                      list(adapters.keys()), with_head=True)
            # model.save_all_adapter_fusions(f"{STORAGE_PATH}/adapter-fusions-output/{adapterfusion_storage_name}/")
            print(f"Saving adapterfusion to {STORAGE_PATH}")
            # ADAPTERS END
        else:
            stagnating_since += 1

        if stagnating_since >= max_stagnating_epochs:
            print("Early stopping!")
            break

    wandb.finish()


def test_adapterfusion(run_id: str, column: str, adapters: dict, backbone: str):
    seed_everything()
    print(f"Starting {run_id} adapterfusion testing with {backbone} for {column}!")

    threshold = 0.50
    max_length = 100
    batch_size = 32

    adapterfusion_name = f"adapterfusion_{backbone}_{column}_{run_id}".replace("/", "-")

    mlb = preprocessing.MultiLabelBinarizer()
    mlb.fit(df_train[column])
    num_labels = len(list(mlb.classes_))

    print("Using whole dataset for testing")
    df_val_subset = df_val
    df_test_subset = df_test

    Y_val = pd.DataFrame(mlb.transform(df_val_subset[column]), columns=list(mlb.classes_))
    Y_test = pd.DataFrame(mlb.transform(df_test_subset[column]), columns=list(mlb.classes_))

    tokenizer = transformers.AutoTokenizer.from_pretrained(
        backbone,
        do_lower_case=True,
        create_token_type_ids_from_sequences=True
    )

    # ADAPTERS BEGIN
    model = AutoAdapterModel.from_pretrained(
        backbone
    )
    model.add_classification_head(','.join(adapters.keys()), num_labels=num_labels, multilabel=True)

    for adapter_key, adapter_value in adapters.items():
        print(f"Loading adapter {adapter_value} for {adapter_key}")
        model.load_adapter(f'{STORAGE_PATH}/adapters-output/{adapter_value}/', load_as=f"{adapter_key}",
                           with_head=False)

    adapter_setup = Fuse(*list(adapters.keys()))
    model.load_adapter_fusion(f"./adapter-fusions-output/{adapterfusion_name}", with_head=True)

    model.set_active_adapters(adapter_setup)

    print(model.adapter_summary())
    print(model.active_adapters)
    print(model.active_head)
    # ADAPTERS END

    model.to(device)

    encodings_val = tokenizer.batch_encode_plus(df_val_subset.excerpt.tolist(), max_length=max_length,
                                                pad_to_max_length=True, truncation=True)
    encodings_test = tokenizer.batch_encode_plus(df_test_subset.excerpt.tolist(), max_length=max_length,
                                                 pad_to_max_length=True)

    input_ids_val = encodings_val['input_ids']
    attention_masks_val = encodings_val['attention_mask']

    input_ids_test = encodings_test['input_ids']
    attention_masks_test = encodings_test['attention_mask']

    validation_inputs = torch.tensor(input_ids_val)
    validation_labels = torch.tensor(Y_val.to_numpy())
    validation_masks = torch.tensor(attention_masks_val)

    test_inputs = torch.tensor(input_ids_test)
    test_labels = torch.tensor(Y_test.to_numpy())
    test_masks = torch.tensor(attention_masks_test)

    validation_data = TensorDataset(validation_inputs, validation_masks, validation_labels)
    validation_sampler = SequentialSampler(validation_data)
    validation_dataloader = DataLoader(validation_data, sampler=validation_sampler, batch_size=batch_size)

    test_data = TensorDataset(test_inputs, test_masks, test_labels)
    test_sampler = SequentialSampler(test_data)
    test_dataloader = DataLoader(test_data, sampler=test_sampler, batch_size=batch_size)

    true_labels, pred_labels = [], []
    val_fusion_attention_scores = []

    model.eval()
    print("Starting Validation iteration")
    with torch.no_grad():
        for i, batch in enumerate(validation_dataloader):
            i % 100 == 0 and print(f"VAL {i}/{len(validation_dataloader)}")

            batch = tuple(t.to(device) for t in batch)
            b_input_ids, b_input_mask, b_labels = batch

            outputs = model(b_input_ids, token_type_ids=None, attention_mask=b_input_mask,
                            output_adapter_fusion_attentions=True)
            val_fusion_attention_scores.append(outputs.adapter_fusion_attentions)

            pred_label = torch.sigmoid(outputs[0]).to('cpu').numpy()

            true_labels.append(b_labels.to('cpu').numpy())
            pred_labels.append(pred_label)

    pred_labels = [item for sublist in pred_labels for item in sublist]
    true_labels = [item for sublist in true_labels for item in sublist]

    pred_bools = [pl > threshold for pl in pred_labels]
    true_bools = [tl == 1 for tl in true_labels]

    val_f1 = f1_score(true_bools, pred_bools, average='macro')

    print(f"{run_id} Validation: {column} F1 Macro Average: {val_f1}")

    true_labels, pred_labels = [], []
    test_fusion_attention_scores = []

    model.eval()
    print("Starting Test iteration")
    with torch.no_grad():
        for i, batch in enumerate(test_dataloader):
            i % 100 == 0 and print(f"TEST {i}/{len(test_dataloader)}")

            batch = tuple(t.to(device) for t in batch)
            b_input_ids, b_input_mask, b_labels = batch

            outputs = model(b_input_ids, token_type_ids=None, attention_mask=b_input_mask,
                            output_adapter_fusion_attentions=True)
            test_fusion_attention_scores.append(outputs.adapter_fusion_attentions)

            pred_label = torch.sigmoid(outputs[0]).to('cpu').numpy()

            true_labels.append(b_labels.to('cpu').numpy())
            pred_labels.append(pred_label)

    pred_labels = [item for sublist in pred_labels for item in sublist]
    true_labels = [item for sublist in true_labels for item in sublist]

    pred_bools = [pl > threshold for pl in pred_labels]
    true_bools = [tl == 1 for tl in true_labels]

    test_f1 = f1_score(true_bools, pred_bools, average='macro')

    print(f"{run_id} TEST {column} F1 Macro Average: {test_f1}")

    report = classification_report(true_bools, pred_bools, target_names=list(mlb.classes_), zero_division=False,
                                   output_dict=True)

    results = pd.DataFrame(report).transpose()

    results.to_csv(f"{STORAGE_PATH}/metrics/{adapterfusion_name}.csv")

    fusion_attention_scores = {
        "val": val_fusion_attention_scores,
        "test": test_fusion_attention_scores
    }

    with open(f"{STORAGE_PATH}/attention-scores/{adapterfusion_name}.bin", "wb") as h:
        pickle.dump(fusion_attention_scores, h, protocol=pickle.HIGHEST_PROTOCOL)

    with open(f"{STORAGE_PATH}/predictions/{adapterfusion_name}.bin", "wb") as h:
        pickle.dump({
            "true_bools": true_bools,
            "pred_bools": pred_bools,
            "labels": mlb.classes_
        }, h, protocol=pickle.HIGHEST_PROTOCOL)


for learning_rate in [2e-4, 1e-4, 9e-5]:
    for col in COLUMNS:
        run_id = get_run_id()
        train_adapterfusion(run_id, col, adapters, XLM_ROBERTA, learning_rate)
        test_adapterfusion(run_id, col, adapters, XLM_ROBERTA)

