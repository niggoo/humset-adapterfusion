import pickle
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import os

from sklearn.metrics import f1_score


def load_predictions(adapterfusion_name):
    with open(f"./predictions/{adapterfusion_name}.bin", "rb") as h:
        return pickle.load(h)


name = "adapter_microsoft-xtremedistil-l6-h256-uncased_subpillars_2d_run_1682101273"

predictions = load_predictions(name)
predictions["true_bools"] = np.array(predictions["true_bools"])
predictions["pred_bools"] = np.array(predictions["pred_bools"])


top_labels = list(map(lambda x: x.split('->')[0], predictions["labels"]))

unique_top_labels = dict.fromkeys(set(top_labels), [])

for i, top_label in enumerate(top_labels):
    unique_top_labels[top_label] = unique_top_labels[top_label] + [i]


combined_true_bools = []
combined_pred_bools = []

for label in unique_top_labels:

    combined_true_bools.append(
        np.logical_or.reduce(predictions["true_bools"][:, unique_top_labels[label]], axis=(1))
    )
    combined_pred_bools.append(
        np.logical_or.reduce(predictions["pred_bools"][:, unique_top_labels[label]], axis=(1))
    )

combined_true_bools = np.array(combined_true_bools)
combined_pred_bools = np.array(combined_pred_bools)

test_f1 = f1_score(combined_true_bools, combined_pred_bools, average='macro')

print(f"F1 Macro Average: {test_f1}")

print("Hi")