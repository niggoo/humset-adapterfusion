from common import *
from common_adapterfusion import *


class AdapterFusionMultiHeadDataset(Dataset):
    def __init__(self, inputs: Tensor, masks: Tensor, all_col_labels_dict: dict):
        self._inputs = inputs
        self._masks = masks
        self._labels = all_col_labels_dict

    def __getitem__(self, index):
        return (
            self._inputs[index],
            self._masks[index],
            {k: self._labels[k][index] for k in self._labels.keys()}
        )

    def __len__(self):
        return self._inputs.size(0)


def train_adapterfusion_multi_head(run_id: str, adapters: dict, backbone: str, learning_rate):
    seed_everything()
    print(
        f"Starting {run_id} adapterfusion training with <{backbone}> for all columns utilizing multiple classification heads")

    # parameters
    epochs = 20
    max_stagnating_epochs = 5
    max_length = 100
    batch_size = 32
    adapterfusion_name = f"adapterfusion_multi_head_{backbone}_{run_id}".replace("/", "-")

    threshold = 0.50

    gradient_accumulation_steps = 4

    wandb.init(
        project="humset-adapterfusion",
        name=adapterfusion_name,
        mode="online",
        config={
            "use_subset_only": False,
            "type": "adapterfusion",
            "backbone": backbone,
            "column": "all->multi_head",
            "run_id": run_id,
            "max_epochs": epochs,
            "max_stagnating_epochs": max_stagnating_epochs,
            "max_length": max_length,
            "batch_size": batch_size,
            "learning_rate": learning_rate,
            "threshold": threshold,
            "adapters": ', '.join(adapters.values()),
            "sequential": False,
            "multihead": True,
            "gradient_accumulation_steps": gradient_accumulation_steps
        }
    )

    tokenizer = transformers.AutoTokenizer.from_pretrained(
        backbone,
        do_lower_case=True,
        create_token_type_ids_from_sequences=True,
        local_files_only=True
    )

    df_train_subset = df_train
    df_val_subset = df_val

    Y_train = {}
    Y_val = {}
    num_labels = {}

    for column in COLUMNS:
        mlb = preprocessing.MultiLabelBinarizer()
        mlb.fit(df_train_subset[column])
        num_labels[column] = len(list(mlb.classes_))

        Y_train[column] = pd.DataFrame(mlb.transform(df_train_subset[column]), columns=list(mlb.classes_))
        Y_val[column] = pd.DataFrame(mlb.transform(df_val_subset[column]), columns=list(mlb.classes_))

    # ADAPTERS BEGIN
    model = AutoAdapterModel.from_pretrained(
        backbone
    )

    for adapter_key, adapter_value in adapters.items():
        print(f"Loading adapter {adapter_value} for {adapter_key}")
        model.load_adapter(f'{STORAGE_PATH}/adapters-output/{adapter_value}/', load_as=f"{adapter_key}",
                           with_head=False)

    adapter_setup = Fuse(*list(adapters.keys()))

    print(f"Creating new adapterfusion-layer: {adapterfusion_name}")
    model.add_adapter_fusion(adapter_setup)

    for column in COLUMNS:
        print(f"Adding classification head for all col {column}_head")
        model.add_classification_head(f"{column}_head", num_labels=num_labels[column], multilabel=True)

    model.active_head = list(map(lambda x: f"{x}_head", COLUMNS))
    model.set_active_adapters(adapter_setup)
    model.train_adapter_fusion(adapter_setup)

    print(model.adapter_summary())
    print(model.active_adapters)
    print(model.active_head)
    # ADAPTERS END

    model.to(device)

    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'gamma', 'beta']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)],
         'weight_decay_rate': 0.01},
        {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)],
         'weight_decay_rate': 0.0}
    ]

    optimizer = transformers.AdamW(optimizer_grouped_parameters, lr=learning_rate, correct_bias=True)

    encodings_train = tokenizer.batch_encode_plus(df_train_subset.excerpt.tolist(), max_length=max_length,
                                                  pad_to_max_length=True, truncation=True)
    encodings_val = tokenizer.batch_encode_plus(df_val_subset.excerpt.tolist(), max_length=max_length,
                                                pad_to_max_length=True, truncation=True)

    input_ids_train = encodings_train['input_ids']
    attention_masks_train = encodings_train['attention_mask']

    input_ids_val = encodings_val['input_ids']
    attention_masks_val = encodings_val['attention_mask']

    train_inputs = torch.tensor(input_ids_train)
    train_labels = {}
    for column in COLUMNS:
        train_labels[column] = torch.tensor(Y_train[column].to_numpy())
    train_masks = torch.tensor(attention_masks_train)

    validation_inputs = torch.tensor(input_ids_val)
    validation_labels = {}
    for column in COLUMNS:
        validation_labels[column] = torch.tensor(Y_val[column].to_numpy())
    validation_masks = torch.tensor(attention_masks_val)

    train_data = AdapterFusionMultiHeadDataset(train_inputs, train_masks, train_labels)
    train_sampler = RandomSampler(train_data)
    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=batch_size)

    validation_data = AdapterFusionMultiHeadDataset(validation_inputs, validation_masks, validation_labels)
    validation_sampler = SequentialSampler(validation_data)
    validation_dataloader = DataLoader(validation_data, sampler=validation_sampler, batch_size=batch_size)

    best_val_f1_sum = 0
    stagnating_since = 0
    for epoch in trange(epochs, desc="Epoch"):
        logging_dict = {}

        # Training
        train_loss = 0
        true_labels, pred_labels = {key: [] for key in COLUMNS}, {key: [] for key in COLUMNS}

        model.train()
        print("Starting new Train iteration")
        for i, batch in enumerate(train_dataloader):
            i % 500 == 0 and print(f"Epoch {epoch}: TRAIN {i}/{len(train_dataloader)} ")

            batch = (batch[0].to(device), batch[1].to(device), {k: batch[2][k].to(device) for k in batch[2].keys()})
            b_input_ids, b_input_mask, labels_dict = batch

            multi_head_outputs = model(b_input_ids, token_type_ids=None, attention_mask=b_input_mask)

            losses = []
            for column, outputs in zip(COLUMNS, multi_head_outputs):
                logits = outputs[0]
                b_labels = labels_dict[column]

                loss = torch.nn.functional.binary_cross_entropy_with_logits(
                    logits.view(-1, num_labels[column]),
                    b_labels.type_as(logits).view(-1, num_labels[column])
                )
                losses.append(loss)

                train_loss += loss.item()

                pred_label = torch.sigmoid(outputs[0]).to('cpu').detach().numpy()

                true_labels[column].append(b_labels.to('cpu').numpy())
                pred_labels[column].append(pred_label)

            overall_loss = functools.reduce(lambda a, b: a + b, losses)
            overall_loss.backward()

            if (i + 1) % gradient_accumulation_steps == 0:
                optimizer.step()
                optimizer.zero_grad()

        logging_dict["train/loss"] = train_loss
        print("Finished training iteration, starting evaluation of train iteration")

        for column in COLUMNS:
            p_labels = [item for sublist in pred_labels[column] for item in sublist]
            t_labels = [item for sublist in true_labels[column] for item in sublist]

            pred_bools = [pl > threshold for pl in p_labels]
            true_bools = [tl == 1 for tl in t_labels]

            train_f1 = f1_score(true_bools, pred_bools, average='macro')
            print(f"{column}: Train F1 Score: {train_f1}")
            logging_dict[f"train/{column}_macro_average_f1"] = train_f1

        # Validation
        val_loss = 0
        val_f1_sum = 0
        true_labels, pred_labels = {key: [] for key in COLUMNS}, {key: [] for key in COLUMNS}

        model.eval()
        print("Starting new Validation iteration")
        with torch.no_grad():
            for i, batch in enumerate(validation_dataloader):
                i % 100 == 0 and print(f"Epoch {epoch}: VAL {i}/{len(validation_dataloader)}")

                batch = (batch[0].to(device), batch[1].to(device), {k: batch[2][k].to(device) for k in batch[2].keys()})
                b_input_ids, b_input_mask, labels_dict = batch

                multi_head_outputs = model(b_input_ids, token_type_ids=None, attention_mask=b_input_mask)

                for column, outputs in zip(COLUMNS, multi_head_outputs):
                    logits = outputs[0]
                    b_labels = labels_dict[column]
                    loss = torch.nn.functional.binary_cross_entropy_with_logits(
                        logits.view(-1, num_labels[column]),
                        b_labels.type_as(logits).view(-1, num_labels[column])
                    )

                    val_loss += loss.item()

                    pred_label = torch.sigmoid(outputs[0]).to('cpu').numpy()

                    true_labels[column].append(b_labels.to('cpu').numpy())
                    pred_labels[column].append(pred_label)

            print("Finished validation iteration, starting evaluation of validation iteration")
        logging_dict["val/loss"] = val_loss

        for column in COLUMNS:
            p_labels = [item for sublist in pred_labels[column] for item in sublist]
            t_labels = [item for sublist in true_labels[column] for item in sublist]

            pred_bools = [pl > threshold for pl in p_labels]
            true_bools = [tl == 1 for tl in t_labels]
            val_f1 = f1_score(true_bools, pred_bools, average='macro')
            print(f"{column}: Val F1 Score: {val_f1}")
            logging_dict[f"val/{column}_macro_average_f1"] = val_f1

            val_f1_sum += val_f1

        print("Logging to wandb")
        wandb.log(logging_dict)

        # Early Stopping
        if val_f1_sum > best_val_f1_sum:
            best_val_f1_sum = val_f1_sum
            stagnating_since = 0
            # ADAPTERS BEGIN
            model.save_adapter_fusion(f"{STORAGE_PATH}/adapter-fusions-output/{adapterfusion_name}",
                                      list(adapters.keys()))

            for column in COLUMNS:
                model.save_head(f"{STORAGE_PATH}/adapter-fusions-output/{adapterfusion_name}/{column}_head",
                                f"{column}_head")

            print(f"Saving adapterfusion and heads to {STORAGE_PATH}")
            # ADAPTERS END
        else:
            stagnating_since += 1

        if stagnating_since >= max_stagnating_epochs:
            print("Early stopping!")
            break

    wandb.finish()


def test_adapterfusion_multi_head(run_id: str, adapters: dict, backbone: str):
    seed_everything()
    print(f"Starting {run_id} adapterfusion testing with {backbone} for all columns with multiple heads!")

    threshold = 0.50
    max_length = 100
    batch_size = 16

    adapterfusion_name = f"adapterfusion_multi_head_{backbone}_{run_id}".replace("/", "-")

    tokenizer = transformers.AutoTokenizer.from_pretrained(
        backbone,
        do_lower_case=True,
        create_token_type_ids_from_sequences=True
    )

    df_val_subset = df_val
    df_test_subset = df_test

    Y_val = {}
    Y_test = {}
    num_labels = {}

    for column in COLUMNS:
        mlb = preprocessing.MultiLabelBinarizer()
        mlb.fit(df_train[column])
        num_labels[column] = len(list(mlb.classes_))

        Y_val[column] = pd.DataFrame(mlb.transform(df_val_subset[column]), columns=list(mlb.classes_))
        Y_test[column] = pd.DataFrame(mlb.transform(df_test_subset[column]), columns=list(mlb.classes_))

    # ADAPTERS BEGIN
    model = AutoAdapterModel.from_pretrained(
        backbone
    )

    for adapter_key, adapter_value in adapters.items():
        print(f"Loading adapter {adapter_value} for {adapter_key}")
        model.load_adapter(f'{STORAGE_PATH}/adapters-output/{adapter_value}/', load_as=f"{adapter_key}",
                           with_head=False)

    adapter_setup = Fuse(*list(adapters.keys()))

    model.load_adapter_fusion(f"./adapter-fusions-output/{adapterfusion_name}/", with_head=False)

    for column in COLUMNS:
        model.load_head(f"./adapter-fusions-output/{adapterfusion_name}/{column}_head")

    model.active_head = list(map(lambda x: f"{x}_head", COLUMNS))
    model.set_active_adapters(adapter_setup)

    print(model.adapter_summary())
    print(model.active_adapters)
    print(model.active_head)
    # ADAPTERS END

    model.to(device)

    encodings_val = tokenizer.batch_encode_plus(df_val_subset.excerpt.tolist(), max_length=max_length,
                                                pad_to_max_length=True, truncation=True)
    encodings_test = tokenizer.batch_encode_plus(df_test_subset.excerpt.tolist(), max_length=max_length,
                                                 pad_to_max_length=True)

    input_ids_val = encodings_val['input_ids']
    attention_masks_val = encodings_val['attention_mask']

    input_ids_test = encodings_test['input_ids']
    attention_masks_test = encodings_test['attention_mask']

    validation_inputs = torch.tensor(input_ids_val)
    validation_labels = {}
    for column in COLUMNS:
        validation_labels[column] = torch.tensor(Y_val[column].to_numpy())
    validation_masks = torch.tensor(attention_masks_val)

    test_inputs = torch.tensor(input_ids_test)
    test_labels = {}
    for column in COLUMNS:
        test_labels[column] = torch.tensor(Y_test[column].to_numpy())
    test_masks = torch.tensor(attention_masks_test)

    validation_data = AdapterFusionMultiHeadDataset(validation_inputs, validation_masks, validation_labels)
    validation_sampler = SequentialSampler(validation_data)
    validation_dataloader = DataLoader(validation_data, sampler=validation_sampler, batch_size=batch_size)

    test_data = AdapterFusionMultiHeadDataset(test_inputs, test_masks, test_labels)
    test_sampler = SequentialSampler(test_data)
    test_dataloader = DataLoader(test_data, sampler=test_sampler, batch_size=batch_size)

    true_labels, pred_labels = {key: [] for key in COLUMNS}, {key: [] for key in COLUMNS}
    val_fusion_attention_scores = []

    model.eval()
    print("Starting Validation iteration")
    with torch.no_grad():
        for i, batch in enumerate(validation_dataloader):
            i % 100 == 0 and print(f"VAL {i}/{len(validation_dataloader)}")

            batch = (batch[0].to(device), batch[1].to(device), {k: batch[2][k].to(device) for k in batch[2].keys()})
            b_input_ids, b_input_mask, labels_dict = batch

            multi_head_outputs = model(b_input_ids, token_type_ids=None, attention_mask=b_input_mask,
                                       output_adapter_fusion_attentions=True)
            val_fusion_attention_scores.append(multi_head_outputs.adapter_fusion_attentions)

            for column, outputs in zip(COLUMNS, multi_head_outputs):
                b_labels = labels_dict[column]

                pred_label = torch.sigmoid(outputs[0]).to('cpu').numpy()

                true_labels[column].append(b_labels.to('cpu').numpy())
                pred_labels[column].append(pred_label)

    for column in COLUMNS:
        p_labels = [item for sublist in pred_labels[column] for item in sublist]
        t_labels = [item for sublist in true_labels[column] for item in sublist]

        pred_bools = [pl > threshold for pl in p_labels]
        true_bools = [tl == 1 for tl in t_labels]
        val_f1 = f1_score(true_bools, pred_bools, average='macro')
        print(f"{run_id} {column}: Val F1 Score: {val_f1}")

    true_labels, pred_labels = {key: [] for key in COLUMNS}, {key: [] for key in COLUMNS}
    test_fusion_attention_scores = []

    model.eval()
    print("Starting Test iteration")
    with torch.no_grad():
        for i, batch in enumerate(test_dataloader):
            i % 100 == 0 and print(f"TEST {i}/{len(test_dataloader)}")

            batch = (batch[0].to(device), batch[1].to(device), {k: batch[2][k].to(device) for k in batch[2].keys()})
            b_input_ids, b_input_mask, labels_dict = batch

            multi_head_outputs = model(b_input_ids, token_type_ids=None, attention_mask=b_input_mask,
                                       output_adapter_fusion_attentions=True)
            test_fusion_attention_scores.append(multi_head_outputs.adapter_fusion_attentions)

            for column, outputs in zip(COLUMNS, multi_head_outputs):
                b_labels = labels_dict[column]

                pred_label = torch.sigmoid(outputs[0]).to('cpu').numpy()

                true_labels[column].append(b_labels.to('cpu').numpy())
                pred_labels[column].append(pred_label)

    for column in COLUMNS:
        p_labels = [item for sublist in pred_labels[column] for item in sublist]
        t_labels = [item for sublist in true_labels[column] for item in sublist]

        pred_bools = [pl > threshold for pl in p_labels]
        true_bools = [tl == 1 for tl in t_labels]
        test_f1 = f1_score(true_bools, pred_bools, average='macro')
        print(f"{run_id} TEST {column}: TEST F1 Score: {test_f1}")

    fusion_attention_scores = {
        "val": val_fusion_attention_scores,
        "test": test_fusion_attention_scores
    }

    with open(f"{STORAGE_PATH}/attention-scores/{adapterfusion_name}.bin", "wb") as h:
        pickle.dump(fusion_attention_scores, h, protocol=pickle.HIGHEST_PROTOCOL)


for learning_rate in [2e-4, 1e-4, 9e-5]:
    run_id = get_run_id()
    train_adapterfusion_multi_head(run_id, adapters, XLM_ROBERTA, learning_rate)
test_adapterfusion_multi_head(run_id, adapters, XLM_ROBERTA)

