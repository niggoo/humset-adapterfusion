from common import *


def train_adapter(run_id: str, column: str, backbone: str):
    seed_everything()
    print(f"Starting {run_id} adapter training with <{backbone}> for <{column}>")

    # parameters
    epochs = 20
    max_stagnating_epochs = 5
    max_length = 100
    batch_size = 64
    learning_rate = 3e-4
    adapter_name = f"adapter_{backbone}_{column}_{run_id}".replace("/", "-")

    threshold = 0.50

    wandb.init(
        project="humset-adapterfusion",
        name=adapter_name,
        mode="online",
        config={
            "use_subset_only": False,
            "type": "adapter",
            "backbone": backbone,
            "column": column,
            "run_id": run_id,
            "max_epochs": epochs,
            "max_stagnating_epochs": max_stagnating_epochs,
            "max_length": max_length,
            "batch_size": batch_size,
            "learning_rate": learning_rate,
            "threshold": threshold,
        }
    )

    print("Using all train data for training!")
    df_train_subset = df_train
    df_val_subset = df_val

    mlb = preprocessing.MultiLabelBinarizer()
    mlb.fit(df_train[column])
    num_labels = len(list(mlb.classes_))

    Y_train = pd.DataFrame(mlb.transform(df_train_subset[column]), columns=list(mlb.classes_))
    Y_val = pd.DataFrame(mlb.transform(df_val_subset[column]), columns=list(mlb.classes_))

    tokenizer = transformers.AutoTokenizer.from_pretrained(
        backbone,
        do_lower_case=True,
        create_token_type_ids_from_sequences=True
    )

    # ADAPTERS BEGIN
    model = AutoAdapterModel.from_pretrained(
        backbone
    )

    model.add_classification_head(adapter_name, num_labels=num_labels, multilabel=True)

    model.add_adapter(adapter_name)
    model.train_adapter(adapter_name)
    model.set_active_adapters(adapter_name)

    print(model.adapter_summary())
    # ADAPTERS END

    model.to(device)

    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'gamma', 'beta']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)],
         'weight_decay_rate': 0.01},
        {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)],
         'weight_decay_rate': 0.0}
    ]

    optimizer = transformers.AdamW(optimizer_grouped_parameters, lr=learning_rate, correct_bias=True)

    encodings_train = tokenizer.batch_encode_plus(df_train_subset.excerpt.tolist(), max_length=max_length,
                                                  pad_to_max_length=True, truncation=True)
    encodings_val = tokenizer.batch_encode_plus(df_val_subset.excerpt.tolist(), max_length=max_length,
                                                pad_to_max_length=True, truncation=True)

    input_ids_train = encodings_train['input_ids']
    attention_masks_train = encodings_train['attention_mask']

    input_ids_val = encodings_val['input_ids']
    attention_masks_val = encodings_val['attention_mask']

    train_inputs = torch.tensor(input_ids_train)
    train_labels = torch.tensor(Y_train.to_numpy())
    train_masks = torch.tensor(attention_masks_train)

    validation_inputs = torch.tensor(input_ids_val)
    validation_labels = torch.tensor(Y_val.to_numpy())
    validation_masks = torch.tensor(attention_masks_val)

    train_data = TensorDataset(train_inputs, train_masks, train_labels)
    train_sampler = RandomSampler(train_data)
    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=batch_size)

    validation_data = TensorDataset(validation_inputs, validation_masks, validation_labels)
    validation_sampler = SequentialSampler(validation_data)
    validation_dataloader = DataLoader(validation_data, sampler=validation_sampler, batch_size=batch_size)

    best_val_f1 = 0
    stagnating_since = 0
    for epoch in trange(epochs, desc="Epoch"):
        # Training
        train_loss = 0
        true_labels, pred_labels = [], []

        model.train()
        print("Starting new Train iteration")
        for i, batch in enumerate(train_dataloader):
            i % 500 == 0 and print(f"Epoch {epoch}: TRAIN {i}/{len(train_dataloader)} ")

            batch = tuple(t.to(device) for t in batch)
            b_input_ids, b_input_mask, b_labels = batch

            outputs = model(b_input_ids, token_type_ids=None, attention_mask=b_input_mask)

            logits = outputs[0]

            loss = torch.nn.functional.binary_cross_entropy_with_logits(
                logits.view(-1, num_labels),
                b_labels.type_as(logits).view(-1, num_labels)
            )

            train_loss += loss.item()

            pred_label = torch.sigmoid(outputs[0]).to('cpu').detach().numpy()

            true_labels.append(b_labels.to('cpu').numpy())
            pred_labels.append(pred_label)

            loss.backward()
            optimizer.step()
            optimizer.zero_grad()

        pred_labels = [item for sublist in pred_labels for item in sublist]
        true_labels = [item for sublist in true_labels for item in sublist]

        pred_bools = [pl > threshold for pl in pred_labels]
        true_bools = [tl == 1 for tl in true_labels]
        train_f1 = f1_score(true_bools, pred_bools, average='macro')

        # Validation
        val_loss = 0
        true_labels, pred_labels = [], []

        model.eval()
        print("Starting new Validation iteration")
        with torch.no_grad():
            for i, batch in enumerate(validation_dataloader):
                i % 100 == 0 and print(f"Epoch {epoch}: VAL {i}/{len(validation_dataloader)}")

                batch = tuple(t.to(device) for t in batch)
                b_input_ids, b_input_mask, b_labels = batch

                outputs = model(b_input_ids, token_type_ids=None, attention_mask=b_input_mask)

                logits = outputs[0]

                loss = torch.nn.functional.binary_cross_entropy_with_logits(
                    logits.view(-1, num_labels),
                    b_labels.type_as(logits).view(-1, num_labels)
                )

                val_loss += loss.item()

                pred_label = torch.sigmoid(outputs[0]).to('cpu').numpy()

                true_labels.append(b_labels.to('cpu').numpy())
                pred_labels.append(pred_label)

        pred_labels = [item for sublist in pred_labels for item in sublist]
        true_labels = [item for sublist in true_labels for item in sublist]

        pred_bools = [pl > threshold for pl in pred_labels]
        true_bools = [tl == 1 for tl in true_labels]
        val_f1 = f1_score(true_bools, pred_bools, average='macro')

        wandb.log({
            'train/loss': train_loss,
            'train/macro_average_f1': train_f1,
            'val/loss': val_loss,
            'val/macro_average_f1': val_f1,
        })

        # Early Stopping
        if val_f1 > best_val_f1:
            best_val_f1 = val_f1
            stagnating_since = 0
            # ADAPTERS BEGIN
            model.save_adapter(f"{STORAGE_PATH}/adapters-output/{adapter_name}/", adapter_name, with_head=True)
            # model.save_all_adapters(f"{STORAGE_PATH}/adapters-output/")
            print(f"Saving adapter to {STORAGE_PATH}")
            # ADAPTERS END
        else:
            stagnating_since += 1

        if stagnating_since >= max_stagnating_epochs:
            print("Early stopping!")
            break

    wandb.finish()


def test_adapter(run_id: str, column: str, backbone: str):
    seed_everything()
    print(f"Starting {run_id} adapter testing with {backbone} for {column}!")

    threshold = 0.50
    max_length = 100
    batch_size = 64
    adapter_name = f"adapter_{backbone}_{column}_{run_id}".replace("/", "-")

    print("Testing with whole testset")
    df_val_subset = df_val
    df_test_subset = df_test

    mlb = preprocessing.MultiLabelBinarizer()
    mlb.fit(df_train[column])
    num_labels = len(list(mlb.classes_))

    Y_val = pd.DataFrame(mlb.transform(df_val_subset[column]), columns=list(mlb.classes_))
    Y_test = pd.DataFrame(mlb.transform(df_test_subset[column]), columns=list(mlb.classes_))

    tokenizer = transformers.AutoTokenizer.from_pretrained(
        backbone,
        do_lower_case=True,
        create_token_type_ids_from_sequences=True
    )

    encodings_val = tokenizer.batch_encode_plus(df_val_subset.excerpt.tolist(), max_length=max_length,
                                                pad_to_max_length=True, truncation=True)
    encodings_test = tokenizer.batch_encode_plus(df_test_subset.excerpt.tolist(), max_length=max_length,
                                                 pad_to_max_length=True)

    input_ids_val = encodings_val['input_ids']
    attention_masks_val = encodings_val['attention_mask']

    input_ids_test = encodings_test['input_ids']
    attention_masks_test = encodings_test['attention_mask']

    validation_inputs = torch.tensor(input_ids_val)
    validation_labels = torch.tensor(Y_val.to_numpy())
    validation_masks = torch.tensor(attention_masks_val)

    test_inputs = torch.tensor(input_ids_test)
    test_labels = torch.tensor(Y_test.to_numpy())
    test_masks = torch.tensor(attention_masks_test)

    validation_data = TensorDataset(validation_inputs, validation_masks, validation_labels)
    validation_sampler = SequentialSampler(validation_data)
    validation_dataloader = DataLoader(validation_data, sampler=validation_sampler, batch_size=batch_size)

    test_data = TensorDataset(test_inputs, test_masks, test_labels)
    test_sampler = SequentialSampler(test_data)
    test_dataloader = DataLoader(test_data, sampler=test_sampler, batch_size=batch_size)

    # ADAPTERS BEGIN
    model = AutoAdapterModel.from_pretrained(
        backbone
    )

    model.add_classification_head(adapter_name, num_labels=num_labels, multilabel=True)

    model.load_adapter(f'{STORAGE_PATH}/adapters-output/{adapter_name}/', with_head=True)
    model.set_active_adapters(adapter_name)

    print(model.adapter_summary())
    # ADAPTERS END

    model.to(device)

    true_labels, pred_labels = [], []

    model.eval()
    print("Starting new Validation iteration")
    with torch.no_grad():
        for i, batch in enumerate(validation_dataloader):
            i % 100 == 0 and print(f"VAL {i}/{len(validation_dataloader)}")

            batch = tuple(t.to(device) for t in batch)
            b_input_ids, b_input_mask, b_labels = batch

            outputs = model(b_input_ids, token_type_ids=None, attention_mask=b_input_mask)
            pred_label = torch.sigmoid(outputs[0]).to('cpu').numpy()

            true_labels.append(b_labels.to('cpu').numpy())
            pred_labels.append(pred_label)

    pred_labels = [item for sublist in pred_labels for item in sublist]
    true_labels = [item for sublist in true_labels for item in sublist]

    pred_bools = [pl > threshold for pl in pred_labels]
    true_bools = [tl == 1 for tl in true_labels]

    val_f1 = f1_score(true_bools, pred_bools, average='macro')

    print(f"{run_id} VAL {column} F1 Macro Average: {val_f1}")

    true_labels, pred_labels = [], []

    model.eval()
    print("Starting Test iteration")
    with torch.no_grad():
        for i, batch in enumerate(test_dataloader):
            i % 100 == 0 and print(f"TEST {i}/{len(test_dataloader)}")

            batch = tuple(t.to(device) for t in batch)
            b_input_ids, b_input_mask, b_labels = batch

            outputs = model(b_input_ids, token_type_ids=None, attention_mask=b_input_mask)
            pred_label = torch.sigmoid(outputs[0]).to('cpu').numpy()

            true_labels.append(b_labels.to('cpu').numpy())
            pred_labels.append(pred_label)

    pred_labels = [item for sublist in pred_labels for item in sublist]
    true_labels = [item for sublist in true_labels for item in sublist]

    pred_bools = [pl > threshold for pl in pred_labels]
    true_bools = [tl == 1 for tl in true_labels]

    test_f1 = f1_score(true_bools, pred_bools, average='macro')

    print(f"{run_id} TEST {column} F1 Macro Average: {test_f1}")

    report = classification_report(true_bools, pred_bools, target_names=list(mlb.classes_), zero_division=False,
                                   output_dict=True)
    results = pd.DataFrame(report).transpose()

    results.to_csv(f"{STORAGE_PATH}/metrics/{adapter_name}.csv")

    with open(f"{STORAGE_PATH}/predictions/{adapter_name}.bin", "wb") as h:
        pickle.dump({
            "true_bools": true_bools,
            "pred_bools": pred_bools,
            "labels": mlb.classes_
        }, h, protocol=pickle.HIGHEST_PROTOCOL)


for col in COLUMNS:
    run_id = get_run_id()
    train_adapter(run_id, col, XLM_ROBERTA)
    test_adapter(run_id, col, XLM_ROBERTA)