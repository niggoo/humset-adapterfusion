adapters = {
    "sectors": "adapter_xlm-roberta-base_sectors_run_1682603541",
    "pillars_1d": "adapter_xlm-roberta-base_pillars_1d_run_1682779140",
    "pillars_2d": "adapter_xlm-roberta-base_pillars_2d_run_1682628759",
    "subpillars_1d": "adapter_xlm-roberta-base_subpillars_1d_run_1682836541",
    "subpillars_2d": "adapter_xlm-roberta-base_subpillars_2d_run_1682888197",
}

# adapters = {
#     "sectors": "adapter_microsoft-xtremedistil-l6-h256-uncased_sectors_run_1682081436",
#     "pillars_1d": "adapter_microsoft-xtremedistil-l6-h256-uncased_pillars_1d_run_1682088102",
#     "pillars_2d": "adapter_microsoft-xtremedistil-l6-h256-uncased_pillars_2d_run_1682094437",
#     "subpillars_1d": "adapter_microsoft-xtremedistil-l6-h256-uncased_subpillars_1d_run_1682107899",
#     "subpillars_2d": "adapter_microsoft-xtremedistil-l6-h256-uncased_subpillars_2d_run_1682101273",
# }